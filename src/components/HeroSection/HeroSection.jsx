import "./HeroSection.css";

const photoGrid = "./../src/assets/img/photo-grid.png";
const title = "Online Experiences"
const paragraph =
  "Join unique interactive activities led by one-of-a-kind hosts—all without leaving home.";

function HeroSection() {
  return (
    <div className="hero-container">
      <img src={photoGrid} alt="" className="photo-grid" />
      <div className="hero-text">
        <h1 className="hero-title">{title}</h1>
        <p className="hero-paragraph">{paragraph}</p>
      </div>
    </div>
  );
}

export default HeroSection;
