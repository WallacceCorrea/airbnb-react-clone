import HeroSection from "../HeroSection/HeroSection";
import ShowCaseSection from "../ShowCaseSection/ShowCaseSection";

import "./MainContent.css";


function MainContent() {
  return (
    <div className="main-content-container">
      <HeroSection />
      <ShowCaseSection />
    </div>
  );
}

export default MainContent;
