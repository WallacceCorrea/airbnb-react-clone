import './Card.css'

function Card(props) {
    let badgeText
    if(props.openSpots === 0 ){
        badgeText = "SOLD OUT"
    } else if (props.location === "Online") {
        badgeText = "ONLINE"
    }

 return (
    <div className="card-container">
        {badgeText && <div className="card-badge">{badgeText}</div>}
        <img className="card-img"  src={`./../../src/assets/img/${props.coverImg}`} alt=""  />
        <div className="card-info">
            <div className="card-info-rate">
                <img className="card-info-img" src="./../../src/assets/img/star.png" alt="" />
                <p className="card-info-rating">{props.stats.rating}</p>
                <p className="card-info-rating">({props.stats.reviewCount})</p>
                <p className="card-info-rating">• {props.location}</p>
            </div>
            <p className="card-info-title">{props.title}</p>
            <p className="card-info-cost"><strong>From ${props.price}</strong>/{props.person}</p>
        </div>
    </div>
    
 )
}

export default Card;