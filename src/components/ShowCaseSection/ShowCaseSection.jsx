import "./ShowCaseSection.css";
import cardsData from "./../../assets/db";

import Card from "../Card/Card";

function getCards() {
  const cardsArray = cardsData.map((card) => {
    return (
      <Card
        key={card.id}
        {...card}
      />
    );
  });
  return cardsArray;
}

function ShowCaseSection() {
  return <div className="cards-container">{getCards()}</div>;
}

export default ShowCaseSection;
