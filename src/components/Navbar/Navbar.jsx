import "./Navbar.css";

const logo = "./../src/assets/img/logoAirBnb.svg"

function Navbar() {
  return <div className="nav-container">
    <img src={logo} alt="Company's visual identity" className="logo" />
  </div>;
}

export default Navbar;
